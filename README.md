# Colorization Using Optimization

*Programming Assignment 3 for Advanced Computer Graphics at Tsinghua University 2018 Autumn*

Project website: [http](http://www.cs.huji.ac.il/~yweiss/Colorization/)[://www.cs.huji.ac.il/~yweiss/Colorization](http://www.cs.huji.ac.il/~yweiss/Colorization/)[/](http://www.cs.huji.ac.il/~yweiss/Colorization/) 

<video src="./report/combine.mp4"></video>

![Screen Shot 2018-12-03 at 11.17.03](./Screen Shot 2018-12-03 at 11.17.03.png)

![Screen Shot 2018-12-03 at 11.17.11](./Screen Shot 2018-12-03 at 11.17.11.png)

![Screen Shot 2018-12-03 at 11.17.20](./Screen Shot 2018-12-03 at 11.17.20.png)

![Screen Shot 2018-12-03 at 11.17.34](./Screen Shot 2018-12-03 at 11.17.34.png)
